FROM ubuntu:latest
MAINTAINER SeebaSH
RUN apt-get update && \
	apt-get install -y python3
COPY hello.py /home/hello.py
WORKDIR /home
ENTRYPOINT ["/usr/bin/python3", "./hello.py"]
